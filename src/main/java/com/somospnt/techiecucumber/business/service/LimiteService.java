package com.somospnt.techiecucumber.business.service;

import com.somospnt.techiecucumber.domain.Cliente;
import com.somospnt.techiecucumber.repository.ClienteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@RequiredArgsConstructor
@Service
public class LimiteService {
    
    private static final BigDecimal SUELDO_MINIMO = new BigDecimal(10000);

    private final ClienteRepository clienteRepository;

    public int obtenerLimite(Long idCliente) {
        return clienteRepository.findById(idCliente).map(cliente -> {
            if (!tieneDeuda(cliente) && tieneSueldoMinimo(cliente)){
                return 10000;
            } else {
                return 0;
            }
        }).get();
    }

    private boolean tieneDeuda(Cliente cliente) {
        return cliente.getDeuda().compareTo(BigDecimal.ZERO) != 0;
    }

    private boolean tieneSueldoMinimo(Cliente cliente) {
        return cliente.getSueldoPesos().compareTo(SUELDO_MINIMO) >= 0;
    }
}
