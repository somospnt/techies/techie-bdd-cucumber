package com.somospnt.techiecucumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieCucumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechieCucumberApplication.class, args);
	}

}
