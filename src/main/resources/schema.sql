DROP TABLE cliente IF EXISTS;

CREATE TABLE cliente (
    id INT IDENTITY,
    nombre VARCHAR,
    deuda DECIMAL,
    sueldo_pesos DECIMAL
);