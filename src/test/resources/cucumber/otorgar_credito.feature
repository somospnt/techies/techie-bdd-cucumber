# language: es

  Característica: Sacar credito
    Como Banco
    Quiero poner un limite de otorgamiento al cliente segun sus condiciones financieras
    Para reducir el riesgo de otorgarle mas plata de la que puede pagar

    Escenario: cliente con deuda

      Dado un cliente con deuda
      Cuando pregunto mi limite
      Entonces tengo un limite de 0 pesos

    Escenario: cliente sin deuda

      Dado un cliente sin deuda
      Cuando pregunto mi limite
      Entonces tengo un limite de 10000 pesos

    Esquema del escenario: cliente con sueldo <sueldo>

      Dado un cliente con un sueldo de <sueldo> pesos
      Cuando pregunto mi limite
      Entonces tengo un limite de <limite> pesos

      Ejemplos:
        | sueldo | limite |
        | 10001  | 10000  |
        | 10000  | 10000  |
        | 9999   | 0      |