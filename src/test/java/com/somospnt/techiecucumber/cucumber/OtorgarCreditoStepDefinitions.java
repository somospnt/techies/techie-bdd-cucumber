package com.somospnt.techiecucumber.cucumber;

import com.somospnt.techiecucumber.business.service.LimiteService;
import com.somospnt.techiecucumber.domain.Cliente;
import com.somospnt.techiecucumber.repository.ClienteRepository;
import io.cucumber.java.Before;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;

@SpringBootTest
public class OtorgarCreditoStepDefinitions  {

    @Autowired
    private LimiteService limiteService;

    @Autowired
    private ClienteRepository clienteRepository;

    private Cliente cliente;
    private int limite;

    @Dado("un cliente con deuda")
    public void un_cliente_con_deuda() {
        cliente = new Cliente();
        cliente.setNombre("Pepe");
        cliente.setDeuda(new BigDecimal(1));
        cliente.setSueldoPesos(new BigDecimal(100000));
        clienteRepository.save(cliente);
    }

    @Dado("un cliente sin deuda")
    public void un_cliente_sin_deuda() {
        cliente = new Cliente();
        cliente.setNombre("Pepe");
        cliente.setDeuda(new BigDecimal(0));
        cliente.setSueldoPesos(new BigDecimal(100000));
        clienteRepository.save(cliente);
    }

    @Dado("un cliente con un sueldo de {int} pesos")
    public void un_cliente_con_un_sueldo_de_pesos(Integer sueldo) {
        cliente = new Cliente();
        cliente.setNombre("Pepe");
        cliente.setDeuda(new BigDecimal(0));
        cliente.setSueldoPesos(new BigDecimal(sueldo));
        clienteRepository.save(cliente);
    }

    @Cuando("pregunto mi limite")
    public void pregunto_mi_limite() {
        limite = limiteService.obtenerLimite(cliente.getId());
    }

    @Entonces("tengo un limite de {int} pesos")
    public void tengo_un_limite_de_pesos(Integer limiteEsperado) {
        assertEquals(limite, (long) limiteEsperado);
    }

    @Before
    public void cleanUp() {
        clienteRepository.deleteAll();
    }

}
